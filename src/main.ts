import { splitRangeIntoThreads } from './splitRangesIntoThreads';
import os from 'node:os';
import { createPromisedWorker } from './createPromisedWorker';

const NUMBER_OF_THREADS = os.availableParallelism();
const RANGE_MIN = 0;
const RANGE_MAX = 1e7;

(async function runMain(): Promise<void> {
  try {
    console.time('time threads');

    const workers = splitRangeIntoThreads(
      NUMBER_OF_THREADS,
      RANGE_MIN,
      RANGE_MAX,
    ).map(createPromisedWorker);

    const responses = await Promise.all(workers);

    console.log(responses.flat());
    console.log(responses.flat().length);
    console.timeEnd('time threads');
  } catch (error) {
    console.error(error);
  }
})();
