import { isPrime } from './isPrime';

export const getPrimes = (min: number, max: number): number[] => {
  const primes = [];
  for (let i = min; i <= max; i++) {
    if (isPrime(i)) {
      primes.push(i);
    }
  }
  return primes;
};
