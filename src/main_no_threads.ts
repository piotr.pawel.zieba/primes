import { getPrimes } from './getPrimes';

const RANGE_MIN = 0;
const RANGE_MAX = 1e7;

console.time('no threads');
getPrimes(RANGE_MIN, RANGE_MAX);
console.timeEnd('no threads');
