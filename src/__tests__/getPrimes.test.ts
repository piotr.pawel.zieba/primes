import { getPrimes } from '../getPrimes';

describe('getPrimes function', () => {
  it('getPrimes for a given range', () => {
    expect(getPrimes(0, 10)).toEqual([2, 3, 5, 7]);
  });
});
