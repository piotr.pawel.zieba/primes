import { splitRangeIntoThreads } from '../splitRangesIntoThreads';

describe('splitIntoRanges function', () => {
  it('splits given range into seperate blocks', () => {
    expect(splitRangeIntoThreads(4, 0, 20).length).toEqual(4);
  });

  it('splits given range into valid seperate blocks', () => {
    expect(splitRangeIntoThreads(4, 0, 19)).toEqual([
      [0, 1, 2, 3, 4],
      [5, 6, 7, 8, 9],
      [10, 11, 12, 13, 14],
      [15, 16, 17, 18, 19],
    ]);
  });
});
