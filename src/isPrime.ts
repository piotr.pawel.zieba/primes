export const isPrime = (num: number): boolean => {
  if (num <= 1) return false; // Numbers less than 2 are not prime
  if (num === 2) return true; // 2 is the only even prime number
  if (num % 2 === 0) return false; // No other even number is prime

  const sqrt = Math.sqrt(num);
  for (let i = 3; i <= sqrt; i += 2) {
    // Check for factors from 3 to sqrt(num)
    if (num % i === 0) return false;
  }

  return true;
};
