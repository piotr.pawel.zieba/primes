import { splitEvery } from 'ramda';

export const splitRangeIntoThreads = (
  numberOfThreads: number,
  min: number,
  max: number,
): number[][] => {
  const range = max - min + 1;

  const wholeRangeNumbers = new Array(range).fill(null).map((_, i) => min + i);
  const slices = Math.ceil(range / numberOfThreads);
  const splitRange = splitEvery(slices, wholeRangeNumbers);

  return splitRange;
};
