import { Worker } from 'worker_threads';
import { promisifyWorker } from './promisifyWorker';

export const createPromisedWorker = (thread: number[]): Promise<number[]> => {
  const [min] = thread;
  const max = min + thread.length - 1;

  return promisifyWorker(
    new Worker(__dirname + '/generatePrimesWorker.js', {
      workerData: {
        min,
        max,
      },
    }),
  );
};
