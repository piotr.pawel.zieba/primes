import { Worker } from 'worker_threads';

export const promisifyWorker = (worker: Worker): Promise<number[]> =>
  new Promise((resolve, reject) => {
    let primes = [];
    worker.on('message', (value) => {
      primes = value;
    });
    worker.on('error', (value) => reject(value));
    worker.on('exit', (code) => {
      if (code !== 0) {
        reject(new Error(`Worker stopped with exit code ${code}`));
      }
      console.log('Worker thread successfully exited');
      resolve(primes);
    });
  });
