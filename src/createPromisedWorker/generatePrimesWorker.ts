import { workerData, parentPort } from 'worker_threads';
import { getPrimes } from '../getPrimes';

const primes = getPrimes(workerData.min, workerData.max);
parentPort.postMessage(primes);
