# Getting started
```yarn / yarn install``` - install  packages  

```yarn build``` - build app  

```yarn start``` - run calculations for primes from range 1-1e7 on all threads  

```yarn start:no-threads``` - run calculations for primes from range 1-1e7 on single thread  

```yarn test``` - run tests